﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WestdriveSettings {

   
    public static string language;
    public static AudioClip radioTalkEnglish;
    public static AudioClip radioTalkGerman;
    public static AudioClip taxiDriverMonolougeEnglish;
    public static AudioClip taxiDriverMonolougeGerman;
    private static float progress;
    public static float Progress
    {
        get
        {
            return progress;
        }
        set
        {
            if(value >= 0f && value <= 1f)
            {
                progress = value;
            }
        }
    }
    private static float randomCodeMax = 100;
    public static float RandomCodeMax
    {
        get
        {
            return randomCodeMax;
        }
        set
        {
            if(value >= 1)
                randomCodeMax = value;
        }
    }
    private static int participantCode = 0;
    public static int ParticipantCode
    {
        get
        {
            if (participantCode == 0)
            {
                participantCode = Mathf.FloorToInt(Random.Range(0f, 100f));
                return participantCode;
            }
            else return participantCode;
        }
    }
    private static int participantUID = 0;
    public static int ParticipantUID
    {
        get
        {
            if (participantUID == 0)
            {
                
                participantUID = System.DateTime.UtcNow.Ticks.ToString().GetHashCode();
                return participantUID;
            }
            else return participantUID;
        }
    }
}
