﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using UnityEngine.Video;
public class IdleController : MonoBehaviour {
    [Header("Components")]
    public CanvasGroup announcement;
    public CanvasGroup screen;
    public GameObject mainController;

    [Space]
    [Header("parameters")]
    [Range(0, 1)]
    public float fadeSpeed = 0.05f;
    public float frameWaitSpeed = 0.01f;
    
    // Use this for initialization

    void Start () {

        screen.alpha = 0;
        screen.gameObject.SetActive(false);
        announcement.alpha = 0;
        StartCoroutine(fadeInAnnouncement());
	}
  
    // Update is called once per frame
    void Update () {
        

    }
    public void UserPresent()
    {
        if (!mainController.activeSelf)
        {
            Debug.Log("preparing load");
            StartCoroutine(fadeOutAnnouncement());
        }
    }
    public void UserNotPresent()
    {
        if (!mainController.GetComponent<ExperimentLoader>().isLoading)
        {
            Debug.Log("loading halted");
            StartCoroutine(fadeOutScreen());
        }
        else
        {
            Debug.Log("Loading already started, cannot halt procedure");
        }
    }
    IEnumerator fadeInAnnouncement()
    {
        announcement.gameObject.SetActive(true);
        float fadeAlpha = 0f;
        while (fadeAlpha <= 1f)
        {
            announcement.alpha = fadeAlpha;
            fadeAlpha += fadeSpeed;
            yield return new WaitForSeconds(frameWaitSpeed);
        }
        announcement.alpha = 1f;
        yield return null;
    }
    IEnumerator fadeOutAnnouncement()
    {
        float fadeAlpha = 1f;
        while (fadeAlpha >= 0f)
        {
            announcement.alpha = fadeAlpha;
            fadeAlpha -= fadeSpeed;
            yield return new WaitForSeconds(frameWaitSpeed);
        }
        announcement.alpha = 0f;
        announcement.gameObject.SetActive(false);
        screen.gameObject.SetActive(false);
        StartCoroutine(fadeInScreen());
        yield return null;
    }
    IEnumerator fadeInScreen()
    {
        screen.gameObject.SetActive(true);
        float fadeAlpha = 0f;
        while (fadeAlpha <= 1f)
        {
            screen.alpha = fadeAlpha;
            fadeAlpha += fadeSpeed;
            yield return new WaitForSeconds(frameWaitSpeed);
        }
        screen.alpha = 1f;
        mainController.SetActive(true);
        yield return null;
    }
    IEnumerator fadeOutScreen()
    {
        float fadeAlpha = 1f;
        while (fadeAlpha >= 0f)
        {
            screen.alpha = fadeAlpha;
            fadeAlpha -= fadeSpeed;
            yield return new WaitForSeconds(frameWaitSpeed);
        }
        screen.alpha = 0f;
        mainController.SetActive(false);
        screen.gameObject.SetActive(false);
        StartCoroutine(fadeInAnnouncement());
        yield return null;
    }
}
