﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightControl : MonoBehaviour {

    // Use this for initialization
    [Header("Side Controls")]
    public TrafficLight enterOne;
    public TrafficLight enterOneInfront;
    public TrafficLight enterTwo;
    public TrafficLight enterTwoInfront;
    [Space]
    [Header("Light Controls")]
    public float straightGreenTimer = 10f;
    public float yellowLightTimer = 2f;
    public float turnLeftGreenTimer = 5f;
    private bool systemLock = false;
    
    public void Init()
    {
       
        enterOne.straighGreen = false;
        enterOne.straighYellow = false;
        enterOne.turnLeftGreen = false;
        enterOneInfront.straighGreen = false;
        enterOneInfront.straighYellow = false;
        enterOneInfront.turnLeftGreen = false;
        enterTwo.straighGreen = false;
        enterTwo.straighYellow = false;
        enterTwo.turnLeftGreen = false;
        enterTwoInfront.straighGreen = false;
        enterTwoInfront.straighYellow = false;
        enterTwoInfront.turnLeftGreen = false;
       
    }
    private void OnEnable()
    {
        Init();
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        enterOne.straighGreen = false;
        enterOne.straighYellow = false;
        enterOne.turnLeftGreen = false;
        enterOneInfront.straighGreen = false;
        enterOneInfront.straighYellow = false;
        enterOneInfront.turnLeftGreen = false;
        enterTwo.straighGreen = false;
        enterTwo.straighYellow = false;
        enterTwo.turnLeftGreen = false;
        enterTwoInfront.straighGreen = false;
        enterTwoInfront.straighYellow = false;
        enterTwoInfront.turnLeftGreen = false;
    }
    
    private void SideOneStraightGreen()
    {
        systemLock = true;
        enterOne.straighGreen = true;
        enterOneInfront.straighGreen = true;
        Invoke("SideOneStraightYellow", straightGreenTimer);
    }
    private void SideOneStraightYellow()
    {
        enterOne.straighGreen = false;
        enterOneInfront.straighGreen = false;
        enterOne.straighYellow = true;
        enterOneInfront.straighYellow = true;
        Invoke("SideOneStraightRed", yellowLightTimer);
    }
    private void SideOneStraightRed()
    {
        
        enterOne.straighYellow = false;
        enterOneInfront.straighYellow = false;
        SideOneTrueLeftGreen();
    }
    private void SideOneTrueLeftGreen()
    {
        enterOne.turnLeftGreen = true;
        enterOneInfront.turnLeftGreen = true;
        Invoke("SideOneTrueLeftYellow", turnLeftGreenTimer);
    }
    private void SideOneTrueLeftYellow()
    {
        enterOne.turnLeftGreen = false;
        enterOneInfront.turnLeftGreen = false;
        enterOne.turnLeftYellow = true;
        enterOneInfront.turnLeftYellow = true;
        Invoke("SideOneTrueLeftRed", yellowLightTimer);
    }
    private void SideOneTrueLeftRed()
    {

        enterOne.turnLeftYellow = false;
        enterOneInfront.turnLeftYellow = false;
        SideTwoStraightGreen();
    }
    private void SideTwoStraightGreen()
    {
        enterTwo.straighGreen = true;
        enterTwoInfront.straighGreen = true;
        Invoke("SideTwoStraightYellow", straightGreenTimer);
    }
    private void SideTwoStraightYellow()
    {
        enterTwo.straighGreen = false;
        enterTwoInfront.straighGreen = false;
        enterTwo.straighYellow = true;
        enterTwoInfront.straighYellow = true;
        Invoke("SideTwoStraightRed", yellowLightTimer);
    }
    private void SideTwoStraightRed()
    {

        enterTwo.straighYellow = false;
        enterTwoInfront.straighYellow = false;
        SideTwoTrueLeftGreen();
    }
    private void SideTwoTrueLeftGreen()
    {
        enterTwo.turnLeftGreen = true;
        enterTwoInfront.turnLeftGreen = true;
        Invoke("SideTwoTrueLeftYellow", turnLeftGreenTimer);
    }
    private void SideTwoTrueLeftYellow()
    {
        enterTwo.turnLeftGreen = false;
        enterTwoInfront.turnLeftGreen = false;
        enterTwo.turnLeftYellow = true;
        enterTwoInfront.turnLeftYellow = true;
        Invoke("SideTwoTrueLeftRed", yellowLightTimer);
    }
    private void SideTwoTrueLeftRed()
    {

        enterTwo.turnLeftYellow = false;
        enterTwoInfront.turnLeftYellow = false;
        systemLock = false;
    }
    private void CycleDone()
    {
        systemLock = false;
    }
    // Update is called once per frame
    void FixedUpdate () {
        if(!systemLock)
            SideOneStraightGreen();
    }
}
