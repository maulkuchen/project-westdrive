﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlmostHaltTrigger : MonoBehaviour {

    public string type = "slow down";
    public float newSpeed = 10f;
    public float newBrakingTorque = 400f;
}
