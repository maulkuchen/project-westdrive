﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcedureProfileList : ScriptableObject
{
    public List<ProcedureProfile> profileList;
}
