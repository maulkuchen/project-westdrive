﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour {
    Animator anim;
    //NavMeshAgent agent;
    [Space]
    [Header("Debug - Calculation Logs")]
    public bool dumpInitialCalculations = false;
    [Space]
    [Header("Path Settings")]
    public BezierSplines path;
    public bool isLoop = false;
    [Tooltip("initial duration for the car to complete the path")]
    public float duration = 0;
    public bool usePathDefaultDuration = true;
    private float[] pathLengthTable;
    private Vector3[] pathNodesTable;
    private float increamentUnit = 0;
    public float startPecentage = 0;
    private float pastDuration;
    // Cars progress on the path
    private float progress;
    [Tooltip("Pedestrian head follow the direction of the path")]
    public bool lookForward = true;
    [Tooltip("Sets if the Pedestrian is going forward in the path or going back from the end of the path")]
    public bool goingForward = true;

    private Vector3 positionToBe;
    private Vector3 directionToHave;
    Vector2 smoothDeltaPosition = Vector2.zero;
    Vector2 velocity = Vector2.zero;
   
    float currentAngularVelocity;
    
    bool shouldMove = false;
    private bool isVisible;
    private string initialGoal;
    Vector3 lastFacing;
    // Use this for initialization
    private void Awake()
    {
        SkinnedMeshRenderer [] meshes = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach(SkinnedMeshRenderer mesh in meshes)
        {
            mesh.enabled = true;
            
        }
    }
    void Start()
    {
        anim = GetComponent<Animator>();
        
        _initialize();
        initiateRenderers();
        progress = startPecentage;
    }
    //private void OnBecameInvisible()
    //{
    //    //Debug.Log("Invisible");
    //    if (!this.CompareTag("Event Object"))
    //    {
    //        isVisible = false;
    //        Renderer[] objectRenderers = GetComponentsInChildren<Renderer>();
    //        foreach (Renderer objectRenderer in objectRenderers)
    //        {
    //            if (objectRenderer.transform.name != this.transform.name)
    //                objectRenderer.enabled = false;
    //        }
    //    }
    //}
    private void initiateRenderers()
    {
        if (!this.CompareTag("Event Object"))
        {
            isVisible = false;
            Renderer[] objectRenderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer objectRenderer in objectRenderers)
            {
                if (objectRenderer.transform.name != this.transform.name)
                    objectRenderer.enabled = false;
            }
        }
        else
            isVisible = true;
    }
    //private void OnBecameVisible()
    //{
    //    //Debug.Log("Visible");
    //    if (!this.CompareTag("Event Object"))
    //    {
    //        isVisible = true;
    //        Renderer[] objectRenderers = GetComponentsInChildren<Renderer>();
    //        foreach (Renderer objectRenderer in objectRenderers)
    //        {
    //            objectRenderer.enabled = true;
    //        }
    //    }
    //}
    public float[] _calculateLengthTableInfo()
    {
        float totalLength = 0f;
        float arrayLength = 1f / (Time.fixedDeltaTime / duration);
        float[] array = new float[Mathf.FloorToInt(arrayLength)];
        array[0] = 0f;
        Vector3 previousPoint = path.GetPoint(0);
        for (int index = 0; index < array.Length; index++)
        {
            float t = ((float)index) / (array.Length - 1);
            Vector3 newPoint = path.GetPoint(t);
            float distaceDifference = (previousPoint - newPoint).magnitude;
            totalLength += distaceDifference;
            array[index] = totalLength;
            previousPoint = newPoint;
        }
        return array;
    }
    public Vector3[] _calculateNodesTableInfo()
    {

        float arrayLength = 1f / (Time.fixedDeltaTime / duration);
        Vector3[] nodes = new Vector3[Mathf.FloorToInt(arrayLength)];
        nodes[0] = path.GetPoint(0);
        Vector3 previousPoint = path.GetPoint(0);
        for (int index = 1; index < nodes.Length; index++)
        {
            float t = ((float)index) / (nodes.Length - 1);
            Vector3 newPoint = path.GetPoint(t);
         
            nodes[index] = newPoint;
            previousPoint = newPoint;
        }
        return nodes;
    }
    private void _initialize()
    {
        initialGoal = this.transform.parent.name;
        if (this.CompareTag("Event Object"))
        {
            switch (this.transform.parent.gameObject.name)
            {
                case "Event1.1":
                    anim.SetInteger("Condition", 1);
                    break;
                case "Event2.1":
                    anim.SetInteger("Condition", 1);
                    break;
                case "Event2.2":
                    anim.SetInteger("Condition", 2);
                    break;
                case "Event3.1":
                    anim.SetInteger("Condition", 3);
                    break;
                case "Event3.2":
                    anim.SetInteger("Condition", 4);
                    break;
                case "Event3.4":
                    anim.SetInteger("Condition", 1);
                    break;
                case "Event4.1 (MSW)":
                    anim.SetInteger("Condition", 1);
                    break;
                case "Event4.2 (MSW)":
                    anim.SetInteger("Condition", 2);
                    break;
                default:

                    break;
            }
            anim.enabled = true;
        }
        else
        {
            anim.SetBool("move", true);
            anim.enabled = true;
        }
        positionToBe = path.GetPoint(startPecentage);
        directionToHave = positionToBe + path.GetDirection(startPecentage);
        if (this.CompareTag("Event Object"))
        {
            transform.position = positionToBe;
            transform.LookAt(directionToHave);
        }
        else
        {
            transform.localPosition = positionToBe;
            transform.LookAt(directionToHave);
        }
        if (usePathDefaultDuration)
            duration = path.duration;
        increamentUnit = Time.fixedDeltaTime / duration;
        pathLengthTable = _calculateLengthTableInfo();
        pathNodesTable = _calculateNodesTableInfo();

    }
    void MoveStep()
    {
        if (goingForward)
        {
            progress += increamentUnit;
            if (progress > 1f)
            {
                if (!isLoop)
                {
                    progress = 1f;
                }
                else
                {
                    progress -= 1f;
                }
            }
        }
        else
        {
            progress -= increamentUnit;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        //Vector3 position = Vector3ArrayExtentions.Sample(pathNodesTable, progress);
        Vector3 position = path.GetPoint(progress);
        positionToBe = position;
        if (lookForward)
        {
            directionToHave = position + path.GetDirection(progress);
        }
    }
    private void FixedUpdate()
    {
        MoveStep();
    }
    void Update()
    {
        shouldMove = true;
       
        //if (isVisible)
        //{
        //    if (!this.CompareTag("Event Object"))
        //    {
        //        anim.SetBool("move", shouldMove);
        //    }
            
        //}
        //if(!isVisible && !this.CompareTag("Event Object"))
        //{
        //    anim.enabled = false;
        //}
        
        if(this.CompareTag("Event Object"))
        {   
            transform.position = positionToBe;
            transform.LookAt(directionToHave);
        }
        else
        {   
            transform.localPosition = positionToBe;
            transform.LookAt(directionToHave);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (this.CompareTag("Event Object"))
        {
           
            if (other.CompareTag("End Event"))
            {
                this.transform.parent.GetComponent<EventHandler>().needMotorControl = false;
            }
            if (other.CompareTag("Destroy Object"))
            {
                this.transform.parent.gameObject.SetActive(false);
                Destroy(this.gameObject);
            }
        }
    }
}


