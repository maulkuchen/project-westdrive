﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;


public class PederstianManager : MonoBehaviour
{

    private int characterPool;
    [Space]
    [Header("Asset List")]
    [SerializeField]
    private AssetLists assets;
    [Space]
    [Header("Looping the path")]
    public bool looping = true;
    [Space]
    [Header("Debug")]
    public bool debugShowSpawnPointsStatistics = false;
    public bool debugShowInitializationMessages = false;
    [Space]
    [Header("internal")]
    public bool loadingDone = false;
    public float assetPopulation = 0;
    public float loadedInstances = 0;
    // Use this for initialization

    private List<GameObject> Characters;
    private GameObject[] spawnPoints;
    private List<GameObject> instantiatedChar;
    private bool locked = false;
    public IEnumerator InitializeAsync()
    {
        loadingDone = false;
        loadedInstances = 0;
        // wait untill all pass instances are destroyed
        while (locked)
        {
            yield return null; 
        }
        // Fetching all destinatios;

        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("pedestrian spawn");

        if (spawnPoints != null)
        {

            if (spawnPoints.Length != 0)
            {
                if (debugShowInitializationMessages)
                    Debug.Log("There are " + spawnPoints.Length + " spawn points registered");
            }
            else
            {
                Debug.LogError("No Spawn Point could be found, please create at least one spawn point");
                yield return null;
            }
        }
        else
        {
            Debug.LogError("No spawn point could be found at all, Please create spawn points first");
            yield return null; 
        }

        //Characters = Resources.LoadAll<GameObject>("KCharacters");
        Characters = new List<GameObject>(assets.Pedestrians);
        if (Characters.Count == 0)
        {
            Debug.LogError("No Character Could be found, Please create at least one object as Character");
            //yield return null;
        }
        else
        {
            assetPopulation = spawnPoints.Length; 
            if (debugShowInitializationMessages)
                Debug.Log("There are " + Characters.Count.ToString() + " Characters found");
            characterPool = Characters.Count;
            instantiatedChar = new List<GameObject>();
            GameObject runtimeCharParent = new GameObject("Characters");
            while (GameObject.Find("Characters") == null)
            {
                yield return null;
            }
            runtimeCharParent.AddComponent<EyeTrackingTarget>();
            runtimeCharParent.transform.parent = this.transform;
            Shuffle(Characters);
            int characterIndexToBeSpawned = 0;
            int population = spawnPoints.Length;
            foreach (GameObject point in spawnPoints)
            {
                GameObject newCharacter = Instantiate<GameObject>(Characters[characterIndexToBeSpawned % characterPool]);
                newCharacter.GetComponent<CharacterManager>().duration = point.GetComponent<SpawnPoint>().duration;
                newCharacter.GetComponent<CharacterManager>().isLoop = point.GetComponent<SpawnPoint>().isLooping;
                newCharacter.GetComponent<CharacterManager>().path = point.GetComponent<SpawnPoint>().path;
                newCharacter.GetComponent<CharacterManager>().startPecentage = point.GetComponent<SpawnPoint>().percentageGone;
                newCharacter.GetComponent<CharacterManager>().goingForward = true;
                newCharacter.GetComponent<CharacterManager>().lookForward = true;
                newCharacter.transform.parent = runtimeCharParent.transform;
                instantiatedChar.Add(newCharacter);
                characterIndexToBeSpawned++;
                loadedInstances++;
                yield return null;
                
            }
        }
        loadingDone = true;
    }
    public void Initialize()
    {
        // Fetching all destinatios;

        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("pedestrian spawn");

        if (spawnPoints != null)
        {

            if (spawnPoints.Length != 0)
            {
                if (debugShowInitializationMessages)
                    Debug.Log("There are " + spawnPoints.Length + " spawn points registered");
            }
            else
            {
                Debug.LogError("No Spawn Point could be found, please create at least one spawn point");
                return;
            }
        }
        else
        {
            Debug.LogError("No spawn point could be found at all, Please create spawn points first");
            return;
        }




        Characters = new List<GameObject>(assets.Pedestrians);

        if (Characters.Count == 0)
        {
            Debug.LogError("No Character Could be found, Please create at least one object as Character");
            //yield return null;
        }
        else
        {
            if (debugShowInitializationMessages)
                Debug.Log("There are " + Characters.Count.ToString() + " Characters found");
            characterPool = Characters.Count;
            instantiatedChar = new List<GameObject>();
            GameObject runtimeCharParent = new GameObject("Characters");
            runtimeCharParent.AddComponent<EyeTrackingTarget>();
            runtimeCharParent.transform.parent = this.transform;
            Shuffle(Characters);
            int characterIndexToBeSpawned = 0;
            int population = spawnPoints.Length;
            foreach (GameObject point in spawnPoints)
            {
                GameObject newCharacter = Instantiate<GameObject>(Characters[characterIndexToBeSpawned % characterPool]);
                newCharacter.GetComponent<CharacterManager>().duration = point.GetComponent<SpawnPoint>().duration;
                newCharacter.GetComponent<CharacterManager>().isLoop = point.GetComponent<SpawnPoint>().isLooping;
                newCharacter.GetComponent<CharacterManager>().path = point.GetComponent<SpawnPoint>().path;
                newCharacter.GetComponent<CharacterManager>().startPecentage = point.GetComponent<SpawnPoint>().percentageGone;
                newCharacter.GetComponent<CharacterManager>().goingForward = true;
                newCharacter.GetComponent<CharacterManager>().lookForward = true;
                newCharacter.transform.parent = runtimeCharParent.transform;
                //newCharacter.SetActive(false);
                instantiatedChar.Add(newCharacter);
                characterIndexToBeSpawned++;


            }
        }
    }
    public void Init()
    {
      StartCoroutine(InitializeAsync());
    }
    void Awake()
    {

    }
 
    public void ResetSystemAsync()
    {
        locked = true;
        StopAllCoroutines();
        Destroy(GameObject.Find("Characters"));
        locked = false;
        //Init();
    }

    void Shuffle<T>(List<T> array)
    {
        System.Random rnd = new System.Random(System.DateTime.Now.Millisecond);
        int p = array.Count;
        for (int n = p - 1; n > 0; n--)
        {
            int r = rnd.Next(0, n);
            T t = array[r];
            array[r] = array[n];
            array[n] = t;
        }
    }
}
