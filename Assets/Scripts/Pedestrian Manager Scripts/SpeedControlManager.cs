﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpeedControlManager : MonoBehaviour {
    public string state = "pass";
    public float newSpeed = 5;
    public float newBrakingTorque = 500;
    public float newIncreamentUnit = 0.0001f;
    // Use this for initialization
    void Start () {
        Gizmos.color = Color.green;
	}
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position + new Vector3(0, 1, 0), 0.2f);
        

    }
    
    // Update is called once per frame
    void Update () {
        if (state == "pass")
        {
            Gizmos.color = Color.green;
        }
        else if (state == "halt")
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Debug.LogWarning("State is not defined");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.GetComponentInParent<CarEngine>()!= null)
        {

            if (state == "pass")
            {
                transform.parent.GetComponent<NavMeshObstacle>().enabled = true;
            }

        }
    }
    private void OnTriggerExit(Collider other)
    {
        
        if (other.GetComponentInParent<CarEngine>() != null)
        {
            //Debug.Log("Car exited");
            if (state == "pass")
            {
                transform.parent.GetComponent<NavMeshObstacle>().enabled = false;

            }
        }
    }
}
