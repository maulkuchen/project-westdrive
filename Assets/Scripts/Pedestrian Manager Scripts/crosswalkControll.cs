﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class crosswalkControll : MonoBehaviour {

    // Use this for initialization
    private GameObject speedControl;
    private GameObject speedControlOtherSide;
    void Start () {
        speedControl = transform.GetChild(0).gameObject;
        speedControl.GetComponent<BoxCollider>().isTrigger = true;
        //speedControlOtherSide = transform.GetChild(1).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<CharacterManager>() != null)
        {
            
            if (this.GetComponent<NavMeshObstacle>().enabled == false)
            {
                speedControl.GetComponent<SpeedControlManager>().gameObject.tag = "Pedestrian";
                speedControl.GetComponent<SpeedControlManager>().state = "halt";
                speedControl.GetComponent<BoxCollider>().isTrigger = false;
            }
                //speedControl.GetComponent<BoxCollider>().isTrigger = false;
            //speedControlOtherSide.GetComponent<SpeedControlManager>().state = "halt";
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharacterManager>() != null)
        {
            speedControl.GetComponent<SpeedControlManager>().gameObject.tag = "crossPathSpeedControll";
            speedControl.GetComponent<SpeedControlManager>().state = "pass";
            speedControl.GetComponent<BoxCollider>().isTrigger = true;
            //speedControlOtherSide.GetComponent<SpeedControlManager>().state = "pass";
        }
    }
}
