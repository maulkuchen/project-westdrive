﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler : MonoBehaviour {

    // Use this for initialization
    public bool needMotorControl = true;
    [Header("Control Object")]
    public GameObject controlObject;
    public GameObject sourceObject;
    public BezierSplines eventPath;
    public GameObject [] blockades;
    public BezierSplines ADVPath;
    [Space]
    [Header("Vocal feedback")]
    public string AVASEnglishFeedbackText = "";
    public string AVASGermanFeedbackText = "";
    [Space]
    [Header("Event Delay")]
    public float delay = 0;
    [Space]
    [Header("Debug")]
    public string objectInfo = "";
    
	void Start () {
        if (sourceObject == null)
        {
            objectInfo = "Simple Control";
        }
        else
        {
            if (sourceObject.GetComponent<CarEngine>() != null)
            {
                if (eventPath != null)
                {
                    controlObject = Instantiate<GameObject>(sourceObject);
                    controlObject.GetComponent<CarEngine>().isLoop = false;
                   
                    controlObject.GetComponent<CarEngine>().path = eventPath;
                    controlObject.GetComponent<CarEngine>().startPecentage = 0;
               
                    
                    controlObject.tag = "Event Object";
                    controlObject.transform.parent = this.transform;
                    controlObject.SetActive(false);
                }
                objectInfo = "Car control";
            }
            if (sourceObject.GetComponent<Animator>() != null)
            {
                if (eventPath != null)
                {
                    controlObject = Instantiate<GameObject>(sourceObject);
                    controlObject.GetComponent<CharacterManager>().isLoop = false;
                    controlObject.GetComponent<CharacterManager>().usePathDefaultDuration = true;
                    controlObject.GetComponent<CharacterManager>().path = eventPath;
                    controlObject.GetComponent<CharacterManager>().startPecentage = 0;
                    controlObject.GetComponent<CharacterManager>().goingForward = true;
                    controlObject.GetComponent<CharacterManager>().lookForward = true;
                    controlObject.tag = "Event Object";
                    controlObject.transform.parent = this.transform;
                    controlObject.SetActive(false);
                }
                objectInfo = "Pedestrian Control";
            }
        }
        if(blockades != null)
        {
            foreach(GameObject blockade in blockades)
            {
                blockade.SetActive(false);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void RunEvent()
    {
        Invoke("Event", delay);
    }
    private void Event()
    {
        Debug.Log("Running an Event!");
        if (blockades != null)
        {
            foreach (GameObject blockade in blockades)
            {
                blockade.SetActive(true);
            }
        }
        controlObject.SetActive(true);
    }


    private void OnDisable()
    {
        //foreach(GameObject child in this.GetComponentsInChildren<GameObject>())
        //{
        //    child.SetActive(false);
        //}
    }
}
