﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventGate : MonoBehaviour {

    [Space]
    [Header("Control")]
    public EventHandler scenario;
   
    [TextArea]
    public string scenarioDescription;
    [Space]
    [Header("AVAS Script")]
    public string AVASEnglishText = "";
    public string AVASGermanText = "";
    // Use this for initialization
    void Start () {
        scenario = this.transform.GetComponentInParent<EventHandler>();
	}

    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("ADV") && other.transform.gameObject.GetComponent<CarEngine>().path == scenario.ADVPath)
        {
            scenario.RunEvent();
        }
    }
    void Update () {
		
	}
}
