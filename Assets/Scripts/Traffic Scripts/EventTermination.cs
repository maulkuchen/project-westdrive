﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTermination : MonoBehaviour {
    public GameObject blockObject;
    public GameObject secondBlockObject;
    public GameObject trafficLight;
    public bool selfDestructOnEventEnd = true;
	// Use this for initialization
	void Start () {
        if (blockObject != null)
            blockObject.SetActive(true);
        if (secondBlockObject != null)
            secondBlockObject.SetActive(true);
        if (trafficLight != null)
            trafficLight.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Event Object"))
        {
            if (blockObject != null)
                Destroy(blockObject);
            if (secondBlockObject != null)
                Destroy(secondBlockObject);
            if (trafficLight != null)
                trafficLight.SetActive(true);
            if (selfDestructOnEventEnd)
                Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Event Object"))
        {
            collision.gameObject.SetActive(false);
        }
    }
}
