﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour {

    public float levelSpeed = 70f;
    public float levelTorque = 200f;
}
