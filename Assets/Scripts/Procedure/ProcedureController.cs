﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProcedureController : MonoBehaviour
{
    // Start is called before the first frame update
    [Space]
    [Header("Components")]
    public GameObject camera;
    public ProcedureProfile experiment;
    public CarsManager carManager;
    public PederstianManager pedestrianManager;
    public GameObject annoucementArea;
    //private GameObject ADV;
    [Space]
    [Header("Interanl Components")]
    private List<string> ADVPaths;
    private List<Object> ADVModules;

    private bool readyToEnd;
    [Space]
    [Header("Anouncement Area")]
    public Transform cameraStand;
    public GameObject announcementBoard;
    [Space]
    [Header("User Presence Control")]
    [Tooltip("Time in seconds which considered as user abondoning the experiment")]
    public float presenceTreshold = 5;
    [Tooltip("Time intervals in seconds to check for user presences")]
    public float controlInterval = 1;
    private bool isEndTriggered = false;
    private bool blockEndTriggered = false;
    private bool userPresent = false;
    private bool userNotPresent = false;
    private bool userCheckLock = false;
    [Space]
    [Header("Internal parameters")]
    private int nextIndex;
    private bool firstBlock = false;
    public float startPercentage = 0f;
    private void Awake()
    {
        annoucementArea.SetActive(true);
        camera = GameObject.Find("Main Camera");
        isEndTriggered = false;
        blockEndTriggered = false;
        userPresent = false;
        userCheckLock = false;
        ADVPaths = new List<string>(experiment.ADVPaths);
        ADVModules = new List<Object>(experiment.ADVModules);
        WestdriveSettings.Progress = 0;
        nextIndex = 0;
        WestdriveSettings.language = experiment.language;
        WestdriveSettings.radioTalkEnglish = experiment.radioTalkEnglish;
        WestdriveSettings.radioTalkGerman = experiment.radioTalkGerman;
        WestdriveSettings.taxiDriverMonolougeEnglish = experiment.taxiDriverMonolougeEnglish;
        WestdriveSettings.taxiDriverMonolougeGerman = experiment.taxiDriverMonolougeGerman;
        if (carManager != null)
            carManager.gameObject.SetActive(true);
        else
            Debug.LogWarning("No car manager in the scene");
        if(pedestrianManager != null)
            pedestrianManager.gameObject.SetActive(true);
        else
            Debug.LogWarning("No pedestrian manager in the scene");
        if (experiment.language == "ENG")
        {
            announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Welcome to the WestDrive. \n Please wait until experiment loads completely";
        }
        if (experiment.language == "DE")
        {
            announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Herzlich willkommen bei WestDrive. \n Bitte warte, bis das Experiment vollständig geladen ist. ";
        }
        camera.transform.position = cameraStand.position;
        camera.transform.rotation = cameraStand.rotation;
        camera.transform.parent = cameraStand;
    }
    private void Start()
    {
        firstBlock = true;
        chooseNextBlock();

    }

    private IEnumerator checkUser()
    {
        float timeElapsed = 0;
        while (timeElapsed < presenceTreshold)
        {
            if (!userPresent)
            {
                timeElapsed += controlInterval;
                yield return new WaitForSeconds(controlInterval);
            }
            else
            {
                userCheckLock = false;
                StopCoroutine(checkUser());
                yield return null;
            }
        }
        if (!userPresent)
        {
#if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
            UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            yield return null;
        }
        yield return null;
    }

    public void UserPresent()
    {
        userPresent = true;
    }
   
    public void UserNotPresent()
    {
        userPresent = false;
        if (!isEndTriggered && !blockEndTriggered && !userCheckLock)
        {
            userCheckLock = true;
            StartCoroutine(checkUser());
        }
        if (isEndTriggered && !blockEndTriggered)
            StartCoroutine(EndProccess());
        
    }
    // Update is called once per frame
    void Update()
    {
        //checking for trigger
        if((WestdriveSettings.Progress > experiment.endTriggerOnPath))
        {
            
            if (ADVPaths.Count != 0)
            {
                StartCoroutine(transferToLobby(1));
            }
                
            else
            {
                
                StartCoroutine(transferToLobby(0));
            }
        }     
        
    }
    private IEnumerator EndProccess()
    {
        Debug.Log("button down");
        Debug.Log("user code =" + WestdriveSettings.ParticipantCode);
        Debug.Log("user uid = " + WestdriveSettings.ParticipantUID);
        if (experiment.useHashValidation)
        {
            int hash = WestdriveSettings.ParticipantUID / WestdriveSettings.ParticipantCode;
            Application.OpenURL("http://www.google.com/control?uid=" + WestdriveSettings.ParticipantUID.ToString() + "&hash=" + hash.ToString());
        }
        else
        {
            Application.OpenURL("http://www.google.com/control?uid=" + WestdriveSettings.ParticipantUID.ToString() + "&hash=false");
        }
        readyToEnd = false;
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
        yield return null;
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
    // mode == 1 -> end block, mode == 0 -> end experiment
    private IEnumerator transferToLobby(int mode = 1)
    {
        annoucementArea.SetActive(true);
        WestdriveSettings.Progress = 0;
        carManager.ResetSystemAsync();
        pedestrianManager.ResetSystemAsync();
        if (mode == 1)
        {
            if(experiment.language == "ENG")
            {

                announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "You have reached the end of the block, \nPlease Take off the headset";
            }
            if (experiment.language == "DE")
            {
                announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Du hast das Ende vom Block erreicht. \nBitte nimm das Headset ab.";
            }
            camera.transform.position = cameraStand.position;
            camera.transform.rotation = cameraStand.rotation;
            camera.transform.parent = cameraStand;
            
            Destroy(GameObject.FindGameObjectWithTag("ADV"));
            while (!Input.anyKeyDown)
            {
                yield return null;
            }
            chooseNextBlock();
        }
        else if (mode == 0)
        {
            if (experiment.language == "ENG")
            {
                announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "You have reached the end of the experiment.Please take of the headset to continue\n Your unique code is < " + WestdriveSettings.ParticipantCode.ToString() + "> \nPlease remember this code before you leave.";
            }
            if (experiment.language == "DE")
            {
                announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Du hast das Ende des Experiments erreichet. Bitte nimm das Headset ab - aber merke dir deinen Code für den Fragebogen. Dein Code ist < " + WestdriveSettings.ParticipantCode.ToString() + ">"; ;
            }
            camera.transform.position = cameraStand.position;
            camera.transform.rotation = cameraStand.rotation;
            camera.transform.parent = cameraStand;
            
            Destroy(GameObject.FindGameObjectWithTag("ADV"));
            isEndTriggered = true;
        }

        yield return null;
    }
    private void chooseNextBlock()
    {
        StopAllCoroutines();
        if (experiment.language == "ENG")
        {
            announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Please wait while experiment is loading";
        }
        if (experiment.language == "DE")
        {
            announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Bitte warte, während das Experiment lädt.";
        }
        if (experiment.randomizeADVPath)
        {
            Shuffle<string>(ref ADVPaths);
            nextIndex = Random.Range(0, 10000);
            if (ADVPaths.Count != 0)
                nextIndex %= ADVPaths.Count;
            else
                nextIndex = 0;
        }
        else
        {
            nextIndex = 0;
        }
        if (experiment.randomizeADVModules)
            Shuffle<Object>(ref ADVModules);
        StartCoroutine(runNextBlock(nextIndex));

    }
    private IEnumerator runNextBlock(int index)
    {
        for(int path_index = 0; path_index < GameObject.Find("Paths").transform.childCount; path_index++)
        {
            GameObject.Find("Paths").transform.GetChild(path_index).gameObject.SetActive(true);
        }
        for (int path_index = 0; path_index < GameObject.Find("Events").transform.childCount; path_index++)
        {
            GameObject.Find("Events").transform.GetChild(path_index).gameObject.SetActive(true);
        }
        for (int path_index = 0; path_index < GameObject.Find("Events").transform.childCount; path_index++)
        {
            if(GameObject.Find("Events").transform.GetChild(path_index).gameObject.GetComponent<EventHandler>().ADVPath != GameObject.Find(ADVPaths[index]).GetComponent<BezierSplines>())
                GameObject.Find("Events").transform.GetChild(path_index).gameObject.SetActive(false);
        }

        Debug.Log("Disabling car paths");
        foreach(StringListDict dict in experiment.disabledCarPaths)
        {
            if(dict.key == ADVPaths[index])
            {
                foreach(string pathName in dict.value)
                {
                    GameObject.Find(pathName).SetActive(false);
                }
            }
        }
        Debug.Log("Disabling pedestrian paths");
        foreach (StringListDict dict in experiment.disabledPedestrianPaths)
        {
            if (dict.key == ADVPaths[index])
            {
                foreach (string pathName in dict.value)
                {
                    GameObject.Find(pathName).SetActive(false);
                }
            }
        }
        
        carManager.Init();
        pedestrianManager.Init();
        blockEndTriggered = false;
        float assetsToLoad = carManager.assetPopulation + pedestrianManager.assetPopulation;
        while(!carManager.loadingDone || !pedestrianManager.loadingDone)
        {
            if (experiment.language == "ENG")
            {
                announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Please wait while experiment is loading\nloading %" + Mathf.FloorToInt(((carManager.loadedInstances + pedestrianManager.loadedInstances) * 100f) / assetsToLoad ).ToString()  + " completed";
            }
            if (experiment.language == "DE")
            {
                announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Bitte warte, während das Experiment lädt.\nloading %" + Mathf.FloorToInt(((carManager.loadedInstances + pedestrianManager.loadedInstances) * 100f) / assetsToLoad).ToString() + " completed";
            }
            yield return null;
        }
        if (experiment.language == "ENG")
        {
            announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "You will be transfered to the car shortly";
        }
        if (experiment.language == "DE")
        {
            announcementBoard.GetComponent<TMPro.TextMeshProUGUI>().text = "Gleich geht es los...";
        }
        yield return null;
        
        GameObject ADV = Instantiate<GameObject>(experiment.ADVPrefab);
        ADV.SetActive(false);
        ADV.GetComponent<CarEngine>().path = GameObject.Find(ADVPaths[index]).GetComponent<BezierSplines>();
        ADV.GetComponent<CarEngine>().startPecentage = startPercentage;
        ADV.SetActive(true);
        camera.transform.position = ADV.GetComponent<CarEngine>().cameraPos.position;
        camera.transform.rotation = ADV.GetComponent<CarEngine>().cameraPos.rotation;
        camera.transform.parent = ADV.GetComponent<CarEngine>().cameraPos;
        annoucementArea.SetActive(false);
        if (ADVModules[index].name == "AVAS")
        {
            ADV.AddComponent<AVAS>();
            
        }
        if(ADVModules[index].name == "RadioTalk")
        {
            
            ADV.GetComponent<CarEngine>().radio.SetActive(true);
        }
        if (ADVModules[index].name == "TaxiDriver")
        {
            ADV.GetComponent<CarEngine>().taxiDriver.SetActive(true);
            ADV.GetComponent<CarEngine>().isTaxiDriver = true;
            
        }
        if (!experiment.reuseADVPath)
        {
            ADVModules.RemoveAt(index);
            ADVPaths.RemoveAt(index); 
        }
        yield return null;
    }

    
    void Shuffle<T>(ref List<T> array)
    {
        System.Random rnd = new System.Random(System.DateTime.Now.Millisecond);
        int p = array.Count;
        for (int n = p - 1; n > 0; n--)
        {
            int r = rnd.Next(0, n);
            T t = array[r];
            array[r] = array[n];
            array[n] = t;
        }
    }
}
