﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpeechLib;
using System.Xml;
using System.IO;

public class AVAS : MonoBehaviour {
    [Tooltip("please make sure that Microsoft's Hedda and Zira voices are installed on the machine")]
    public string language;
    private bool ended = false;
    public float feedbackDelay = 2;
    [Tooltip("Should be true if you want the feedback to be trigger on ADV halt. check false if you want the feedback to be triggered after event ended")]
    public bool feedbackOnHalt = false;
    private string feedbackText;
    private bool introDone = false;
    public bool IntroduntionDone
    {
        get
        {
            return introDone;
        }
    }
    
    private SpVoice voice;
	void Start () {
        
        language = WestdriveSettings.language;
        SpObjectTokenCategory tokenCat = new SpObjectTokenCategory();
        tokenCat.SetId(SpeechLib.SpeechStringConstants.SpeechCategoryVoices, false);
        ISpeechObjectTokens tokens = tokenCat.EnumerateTokens(null, null);
        int n = 0;
        int languageIndex = 0;
        foreach (SpObjectToken item in tokens)
        {   if (language == "DE")
            {
                if (item.GetDescription(0).Contains("Hedda") )
                {
                    languageIndex = n;
                }
                
            }
            if (language == "ENG")
            {
                if (item.GetDescription(0).Contains("Zira"))
                {
                    languageIndex = n;
                }
                
            }
            //Debug.Log("Voice" + n + " ---> " + item.GetDescription(0));
            n++;
        }
        
        voice = new SpVoice();
        voice.Voice = tokens.Item(languageIndex);
        if (language == "ENG")
        {
            voice.Speak("Hello! I'm A.V.A.S., your autonomous driving assistant.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            voice.Speak("I will drive you through the city today.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            voice.Speak("I will inform you about my decisions in traffic so that you feel safe.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            voice.Speak("The approximate driving time is three minutes.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
        }
        else if (language == "DE")
        {
            voice.Speak("Hallo! Ich bin A.V.A.S, dein autonomer Fahrassistent.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            voice.Speak("Ich werde dich heute durch die Stadt fahren", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            voice.Speak("Dabei werde ich dich über meine Entscheidungen im Straßenverkehr informieren, damit du dich sicher fühlst.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            voice.Speak("Die ungefähre Fahrzeit beträgt drei Minuten.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
        }


    }
	
	// Update is called once per frame
	void Update () {
        if(!introDone)
        {
            if (voice.Status.RunningState == SpeechRunState.SRSEDone)
            {
                
                this.gameObject.GetComponent<CarEngine>().waitingForInitialization = false;
                introDone = true;
            }
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EventGate>() != null && this.CompareTag("ADV"))
        {
            if (language == "DE")
            {
                voice.Speak(other.GetComponent<EventGate>().AVASGermanText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
            else if (language == "ENG")
            {
                voice.Speak(other.GetComponent<EventGate>().AVASEnglishText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
        }
        if (other.GetComponent<EventHandler>() != null && this.CompareTag("ADV") && feedbackOnHalt)
        {
            if (language == "DE" && other.GetComponent<EventHandler>().AVASGermanFeedbackText != "")
            {
                feedbackText = other.GetComponent<EventHandler>().AVASGermanFeedbackText;
                Invoke("ReadFeedBackEnglish", feedbackDelay);
                //voice.Speak(other.GetComponent<EventHandler>().AVASGermanFeedbackText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
            else if (language == "ENG" && other.GetComponent<EventHandler>().AVASEnglishFeedbackText != "")
            {
                feedbackText = other.GetComponent<EventHandler>().AVASEnglishFeedbackText;
                Invoke("ReadFeedBackEnglish", feedbackDelay);
                //voice.Speak(other.GetComponent<EventHandler>().AVASEnglishFeedbackText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
        }
        if (other.CompareTag("AVAS End") && this.CompareTag("ADV") && !ended)
        {
            
            if (language == "DE")
            {
                voice.Speak("Wir sind am Ende der Fahrt angekommen. Ich hoffe du konntest die Fahrt genießen. Einen schönen Tag noch und bis hoffentlich bald.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
            else if (language == "ENG")
            {
                voice.Speak("We have reached our destination. I hope you enjoyed the ride. Have a nice day and hopefully see you soon.", SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
            ended = false;
        }


    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<EventHandler>() != null && this.CompareTag("ADV") && !feedbackOnHalt)
        {
            if (language == "DE" && other.GetComponent<EventHandler>().AVASGermanFeedbackText != "")
            {
                feedbackText = other.GetComponent<EventHandler>().AVASGermanFeedbackText;
                Invoke("ReadFeedBackEnglish", feedbackDelay);
                //voice.Speak(other.GetComponent<EventHandler>().AVASGermanFeedbackText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
            else if (language == "ENG" && other.GetComponent<EventHandler>().AVASEnglishFeedbackText != "")
            {
                feedbackText = other.GetComponent<EventHandler>().AVASEnglishFeedbackText;
                Invoke("ReadFeedBackEnglish", feedbackDelay);
                //voice.Speak(other.GetComponent<EventHandler>().AVASEnglishFeedbackText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
            }
        }
    }
    public void ReadFeedBackEnglish()
    {
        Debug.Log("English Invoked");
        voice.Speak(feedbackText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
    }
    public void ReadFeedBackGerman()
    {
        Debug.Log("English German");
        voice.Speak(feedbackText, SpeechVoiceSpeakFlags.SVSFlagsAsync);
    }
    string loadXMLStandalone(string fileName)
    {

        string path = Path.Combine("Resources", fileName);
        path = Path.Combine(Application.dataPath, path);
        Debug.Log("Path:  " + path);
        StreamReader streamReader = new StreamReader(path);
        string streamString = streamReader.ReadToEnd();
        Debug.Log("STREAM XML STRING: " + streamString);
        return streamString;
    }
}
