﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CrazyMinnow.SALSA;
using CrazyMinnow.SALSA.Fuse;
public class TaxiDriver : MonoBehaviour {
    public string language;
    public AudioClip taxiDriverMonolougeEnglish;
    public AudioClip taxiDriverMonolougeGerman;
    private Salsa3D salsa;
	// Use this for initialization
	void Start () {
        salsa = gameObject.GetComponent<Salsa3D>();
        if (language == "ENG")
        {
            if (salsa != null && taxiDriverMonolougeEnglish != null)
            {
                salsa.SetAudioClip(taxiDriverMonolougeEnglish);
                salsa.Play();
            }
        }
        if (language == "DE")
        {
            if (salsa != null && taxiDriverMonolougeGerman != null)
            {
                salsa.SetAudioClip(taxiDriverMonolougeGerman);
                salsa.Play();
            }
        }
    }
    private void OnEnable()
    {
        language = WestdriveSettings.language;
        taxiDriverMonolougeEnglish = WestdriveSettings.taxiDriverMonolougeEnglish;
        taxiDriverMonolougeGerman = WestdriveSettings.taxiDriverMonolougeGerman;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
