Crouch Prone MoCap

This package features a collection of 30 fbx motion capture animations for a bipedal humanoid character moving in and out of crouched and prone positions. 

Crouch and Examine: Crouching down to examine an item of interest on the ground.

Crouch Turn Left: Turning left at a 90, 135, and 180 degree angle while in a stationary crouched position.

Crouch Turn Left 90: Turning left at a 90 degree angle while in a stationary crouched position.

Crouch Turn Left 135: Turning left at a 135 degree angle while in a stationary crouched position.

Crouch Turn Left 180: Turning left at a 180 degree angle while in a stationary crouched position.

Crouch Turn Right: Turning right at a 90, 135, and 180 degree angle while in a stationary crouched position.

Crouch Turn Right 90: Turning right at a 90 degree angle while in a stationary crouched position.

Crouch Turn Right 135: Turning right at a 135 degree angle while in a stationary crouched position.

Crouch Turn Right 180: Turning right at a 180 degree angle while in a stationary crouched position.

Crouch Walk 01: Walking forwards, stopping, then backwards with arms at sides in a crouched position. 

Crouch Walk 01 Forwards: Walking forwards with arms at sides in a crouched position. 

Crouch Walk 01 Backwards: Walking backwards with arms at sides in a crouched position. 

Crouch Walk 02: Walking forwards, stopping, then backwards with arms up cautiously, in a crouched position. 

Crouch Walk 02 Forwards: Walking forwards with arms up cautiously, in a crouched position. 

Crouch Walk 02 Backwards: Walking backwards with arms up cautiously, in a crouched position.

Crouch Walk Turn Left 90: Walking while crouched turning left at a 90 degree angle.

Crouch Walk 03 Forwards: Stealthy walking forwards cautiously with arms up, in a crouched position. 

Crouch Walk Turn Left 135: Walking while crouched turning left at a 135 degree angle.

Crouch Walk Turn Left 180: Walking while crouched turning left at a 180 degree angle.

Crouch Walk Turn Right 90: Walking while crouched turning right at a 90 degree angle.

Crouch Walk Turn Right 135: Walking while crouched turning right at a 135 degree angle.

Crouch Walk Turn Right 180: Walking while crouched turning right at a 180 degree angle.

Lay Down Get Up: Transition from a standing position to a laying down position and then returning to a standing position.

Lay Down: Tranistion from a standing position to a laying down position.

Get Up: Tranisition from a laying down position to a standing position.

Prone Crawl Start: Transition from a standing position to a prone position.

Prone Crawl Mid: Crawling forwards while prone.

Prone Crawl End: Transition from a prone position to a standing position.

Stand/Sit/Stand: Transition from a standing position to a sitting position and back to a standing position.

Stand/Crouch/Prone: Transition from a standing position to a crouched position to a prone position then back to a crouched position before returning to a standing position.

If you have any questions feel free to contact us at support@morromotion.com
