v1.25
   3x Train Traffic Sign
   1x Warning Traffic Sign
   1x Precedence Traffic Sign
   1x Extra Traffic Sign

v1.20
   13x Warning Traffic Sign
   3x Prohibition Traffic Sign
   1x Extra Traffic Sign

v1.10
  30x Informative Traffic Sign
   2x Warning Traffic Sign

v1.00
  16x Mandatory Traffic Sign
  20x Prohibition Traffic Sign
  31x Speed Limit Traffic Sign
  20x Warning Traffic Sign
   3x Precedence Traffic Sign
   1x Extra Traffic Sign